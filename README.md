## Project setup

#### Installation steps

- Create a database and set values for `DB_DATABASE`, `DB_USERNAME`, `DB_PASSWORD` in `.env` file

- Install composer dependencies
```
composer install
```
- Install composer dependencies
```
php artisan storage:link  
```
- Make `avatars` directory in `public/storage`
```
cd ./public/storage && mkdir avatars
```
- Make writable `/storage` directory
```
chmod -R 777 ./storage
```
- Run migrations and seed database
```
php artisan migrate:fresh --seed
```
- Run the application
```
php artisan serve
```
