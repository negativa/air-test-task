@extends('layout')

@section('content')
    <header class="my-6 py-3">
        <h2 class="text-2xl leading-tight">Events</h2>
    </header>
    @foreach($groupedEvents as $month => $events)
        <h4 class="text-lg uppercase text-gray-800 font-thin mt-8 mb-2">{{ $month }}</h4>
        <ul class="ml-1 lg:ml-16">
            @foreach($events as $event)
                <li class="py-2 hover:bg-gray-200">
                    <a href="{{ route('events_show', ['id' => $event->id]) }}">
                        <p class="text-gray-600 text-xs ml-4">{{ $event->started_at->format('d F') }}</p>
                        <p><span class="text-gray-600 font-thin">&rightarrow;</span> {{ $event->name }}</p>
                    </a>
                </li>
            @endforeach
        </ul>
    @endforeach
@endsection
