@extends('layout')

@section('content')
    <p class="text-blue-400 hover:text-blue-600 text-xs my-5 uppercase">
        <a href="{{ route('events_list') }}">&leftarrow; back</a>
    </p>
    <header class="mb-6 py-3">
        <p class="text-gary-700 font-thin my-1 uppercase">{{ $event->started_at->format('d F Y') }}</p>
        <h2 class="text-3xl leading-tight">{{ $event->name }}</h2>
    </header>
    <section class="lg:w-1/2">
        <h3 class="text-gray-600 text-xs mb-6 py-1 border-b border-gray-300 uppercase">Bloggers:</h3>
        <ul>
            @foreach($bloggers as $blogger)
                <li class="my-2 flex items-center justify-between">
                    <div class="flex items-center">
                        <img class="w-12 h-12 rounded-full mr-4"
                             src="/storage/avatars/{{ $blogger->avatar }}"
                             alt="Avatar of {{ $blogger->name }}">
                        <div class="text-sm">
                            <p class="text-gray-900 leading-none">{{ $blogger->name }}</p>
                        </div>
                    </div>
                    <div class="flex text-right">
                        @if(!$loop->first)
                            <form action="{{ route('level_up') }}" method="POST">
                                <input type="hidden" name="eventId" value="{{ $event->id }}">
                                <input type="hidden" name="bloggerId" value="{{ $blogger->id }}">
                                <button type="submit" class="py-1 px-2 hover:bg-gray-200 rounded text-sm">
                                    &uparrow;
                                </button>
                            </form>
                        @endif
                        @if(!$loop->last)
                            <form action="{{ route('level_down') }}" method="POST">
                                <input type="hidden" name="eventId" value="{{ $event->id }}">
                                <input type="hidden" name="bloggerId" value="{{ $blogger->id }}">
                                <button type="submit" class="py-1 px-2 hover:bg-gray-200 rounded text-sm">
                                    &downarrow;
                                </button>
                            </form>
                        @endif
                    </div>
                </li>
            @endforeach
        </ul>
    </section>
@endsection
