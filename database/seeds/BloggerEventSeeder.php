<?php

use App\Blogger;
use App\Event;
use Illuminate\Database\Seeder;

class BloggerEventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $events = Event::all();
        $bloggers = Blogger::all();

        $events->each(function(Event $event) use ($bloggers) {
            $event->bloggers()->attach($bloggers->random(random_int(6,10)));

            $event->bloggers()
                ->each(function ($blogger, $index) use ($event) {
                    $event
                        ->bloggers()
                        ->updateExistingPivot($blogger->id, ['level' => $index]);
                });
        });
    }
}
