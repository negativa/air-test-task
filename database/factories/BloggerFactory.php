<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Blogger;
use Faker\Generator as Faker;

$factory->define(Blogger::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'avatar' => $faker->image('public/storage/avatars',64,64, null, false),
    ];
});
