<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBloggerEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogger_event', function (Blueprint $table) {
            $table->id();
            $table->integer('level')->default(0);
            $table->unsignedBigInteger('blogger_id');
            $table->unsignedBigInteger('event_id');
            $table->timestamps();

            $table->unique(['blogger_id', 'event_id']);

            $table->foreign('blogger_id')
                ->references('id')
                ->on('bloggers')
                ->onDelete('cascade')
            ;
            $table->foreign('event_id')
                ->references('id')
                ->on('events')
                ->onDelete('cascade')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogger_events');
    }
}
