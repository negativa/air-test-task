<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'events_list', 'uses' => 'EventController@index']);
Route::get('/events/{id}', ['as' => 'events_show', 'uses' => 'EventController@show']);

Route::post('/level-up', [
    'as' => 'level_up',
    'uses' => 'EventController@bloggerLevelUp'
]);
Route::post('/level-down', [
    'as' => 'level_down',
    'uses' => 'EventController@bloggerLevelDown'
]);
