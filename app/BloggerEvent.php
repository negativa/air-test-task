<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class BloggerEvent extends Pivot
{
    protected $table = 'blogger_event';

    protected $guarded = [];
}
