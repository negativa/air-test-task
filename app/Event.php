<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'name',
        'started_at',
    ];

    protected $casts = [
        'started_at' => 'datetime',
    ];

    public function bloggers()
    {
        return $this->belongsToMany(Blogger::class)
            ->using(BloggerEvent::class)
            ->withPivot('level')
            ->withTimestamps();
    }
}
