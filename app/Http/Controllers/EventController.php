<?php

namespace App\Http\Controllers;

use App\Blogger;
use App\Event;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function index()
    {
        $events = Event::orderBy('started_at', 'asc')->get();

        $groupedEvents = $events->groupBy(function($event) {
            return $event->started_at->format('F Y');
        });

        return view('events/index', ['groupedEvents' => $groupedEvents]);
    }

    public function show($id)
    {
        $event = Event::findOrFail($id);

        return view('events/show',[
            'event' => $event,
            'bloggers' => $event->bloggers()->get()
                ->sortBy(function ($blogger) {
                    return $blogger->pivot->level;
                })
                ->values(),
        ]);
    }

    public function bloggerLevelUp(Request $request)
    {
        $data = $request->all();
        Blogger::findOrFail($data['bloggerId'])->levelUpInEvent($data['eventId']);

        return redirect(route('events_show', ['id' => $data['eventId']]));
    }

    public function bloggerLevelDown(Request $request)
    {
        $data = $request->all();
        Blogger::findOrFail($data['bloggerId'])->levelDownInEvent($data['eventId']);

        return redirect(route('events_show', ['id' => $data['eventId']]));
    }
}
