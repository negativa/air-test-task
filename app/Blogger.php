<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blogger extends Model
{
    protected $fillable = [
        'name',
        'avatar'
    ];

    public function events()
    {
        return $this->belongsToMany(Event::class)
            ->using(BloggerEvent::class)
            ->withPivot('id', 'level')
            ->withTimestamps();
    }

    public function levelUpInEvent(int $eventId): void {
        $event = $this->events()->find($eventId);

        if (null === $event) {
            return;
        }

        $selfLevel = $this->selfLevelByEvent($event);

        if ($previousBlogger = $this->previousBloggerByEvent($event)) {
            $bloggers = $event->bloggers();
            $bloggers->updateExistingPivot($this->id, ['level' => $selfLevel - 1]);
            $bloggers->updateExistingPivot($previousBlogger->id, ['level' => $selfLevel]);
        }
    }

    public function levelDownInEvent(int $eventId): void {
        $event = $this->events()->find($eventId);

        if (null === $event) {
            return;
        }

        $selfLevel = $this->selfLevelByEvent($event);

        if ($nextBlogger = $this->nextBloggerByEvent($event)) {
            $bloggers = $event->bloggers();
            $bloggers->updateExistingPivot($this->id, ['level' => $selfLevel + 1]);
            $bloggers->updateExistingPivot($nextBlogger->id, ['level' => $selfLevel]);
        }
    }


    public function selfLevelByEvent(Event $event): int
    {
        return $event->pivot->level;
    }

    public function nextBloggerByEvent(Event $event): ?Blogger
    {
        $selfLevel = $this->selfLevelByEvent($event);

        return $event->bloggers()
            ->where('level', '=', ++$selfLevel)
            ->first();
    }

    public function previousBloggerByEvent(Event $event): ?Blogger
    {
        $selfLevel = $this->selfLevelByEvent($event);

        return $event->bloggers()
            ->where('level', '=', --$selfLevel)
            ->first();
    }
}
